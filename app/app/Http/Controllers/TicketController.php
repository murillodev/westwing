<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketRequest;
use App\Service\TicketService;
use App\Service\ClientService;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class TicketController
 *
 * @package App\Http\Controllers
 */
class TicketController extends Controller
{

    /**
     *
     * @var TicketService
     */
    protected $ticketService;

    /**
     * TicketController constructor.
     *
     * @param TicketService $ticketService
     * @param ClientService $clientService
     */
    public function __construct(TicketService $ticketService, ClientService $clientService)
    {
        $this->ticketService = $ticketService;
        $this->clientService = $clientService;
    }

    /**
     *
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $tickets = $this->ticketService->all($request);

        return view('ticket.index', compact('tickets'));
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('ticket.create');
    }

    /**
     *
     * @param  TicketRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TicketRequest $request)
    {
        $return = $this->ticketService->create($request->all());

        if ($return['status'] == '00') {
            return redirect(route('ticket'))->with('success', 'Chamado adicionado com sucesso');
        }

        return redirect()->back()->with('message', $return['message'])->withInput($request->all());
    }

    /**
     *
     * @param  $id
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $this->authorize('excluir_linha');

        $return = $this->lineService->delete($id);

        return \Response::json($return);
    }

    /**
     *
     * @param  $id
     * @return mixed
     */
    public function detail($id)
    {
        $orderID = $this->ticketService->findOrderOfTicket($id);
        $ticket = $this->ticketService->findTicket($id);
        $client = $this->clientService->findById($orderID[0]->client_id);

        $ticketNumber = $orderID[0]->number;
        $ticketTitle = $ticket->title;
        $ticketNote = $ticket->note;
        $ticketDate = $ticket->created_at;

        $clientName = $client->name;
        $clientEmail = $client->email;

        return view('ticket.detail')
            ->with('ticketNumber', $ticketNumber)
            ->with('clientName', $clientName)
            ->with('clientEmail', $clientEmail)
            ->with('ticketTitle', $ticketTitle)
            ->with('ticketNote', $ticketNote)
            ->with('ticketDate', $ticketDate);
    }
}
