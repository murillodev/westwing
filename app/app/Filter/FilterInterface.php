<?php

namespace App\Filter;

/**
 * Interface FilterInterface
 *
 * @package App\Filter
 */
interface FilterInterface
{
    /**
     *
     * @param  $model
     * @return mixed
     */
    public function apply($model);
}
