<?php

namespace App\Filter\Ticket\Conditions;

use App\Filter\FilterInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Number
 *
 * @package App\Filter\Ticket\Conditions
 */
class Number implements FilterInterface
{
    /**
     *
     * @var
     */
    private $value;

    /**
     * Number constructor.
     *
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     *
     * @param  $model
     * @return mixed
     */
    public function apply($model)
    {
        if (strlen($this->value) == 0) {
            return $model;
        }

        $this->value = str_replace("'", '/', $this->value);

        $query = $model->where('number', '=', $this->value);

        return $query;
    }
}
