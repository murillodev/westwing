<?php

namespace App\Repository;

/**
 * Class OrderRepository
 *
 * @package App\Repository
 */
class OrderRepository extends AbstractRepository
{
    /**
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Order';
    }
}
