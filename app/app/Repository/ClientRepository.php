<?php

namespace App\Repository;

/**
 * Class ClientRepository
 *
 * @package App\Repository
 */
class ClientRepository extends AbstractRepository
{
    /**
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Client';
    }
}
