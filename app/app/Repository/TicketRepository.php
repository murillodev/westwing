<?php

namespace App\Repository;

/**
 * Class TicketRepository
 *
 * @package App\Repository
 */
class TicketRepository extends AbstractRepository
{
    /**
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Ticket';
    }

    /**
     *
     * @param  array $columns
     * @return mixed
     */
    public function all(array $columns = array('*'))
    {
        $this->applyCriteria();

        return $this->model
            ->join('order', 'order.id', '=', 'ticket.order_id')
            ->join('client', 'client.id', '=', 'order.client_id')
            ->paginate(5);
    }
}
