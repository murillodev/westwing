<?php

namespace App\Service;

use App\Repository\ClientRepository;

/**
 * Class ClientService
 *
 * @package App\Service
 */
class ClientService
{

    /**
     *
     * @var ClientRepository
     */
    protected $clientRepository;

    /**
     * ClientService constructor.
     *
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     *
     * @param  $email
     * @return mixed
     */
    public function findByEmail($email)
    {
        return $this->clientRepository->findBy('email', $email);
    }

    /**
     *
     * @param  $id
     * @return mixed
     */
    public function findById($id)
    {
        return $this->clientRepository->findBy('id', $id);
    }
}
