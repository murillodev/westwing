<?php

namespace App\Service;

use App\Filter\Ticket\TicketFilter;
use App\Models\Client;
use App\Models\Order;
use App\Models\Ticket;
use App\Repository\TicketRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Dotenv\Exception\ValidationException;

/**
 * Class TicketService
 *
 * @package App\Service
 */
class TicketService
{

    /**
     *
     * @var TicketRepository
     */
    protected $ticketRepository;

    /**
     *
     * @var ClientService
     */
    protected $clientService;

    /**
     *
     * @var OrderService
     */
    protected $orderService;

    /**
     * TicketService constructor.
     *
     * @param TicketRepository $ticketRepository
     * @param ClientService    $clientService
     * @param OrderService     $orderService
     */
    public function __construct(
        TicketRepository $ticketRepository,
        ClientService $clientService,
        OrderService $orderService
    ) {
        $this->ticketRepository = $ticketRepository;
        $this->clientService = $clientService;
        $this->orderService = $orderService;
    }

    /**
     *
     * @param  Request $request
     * @return mixed
     */
    public function allWithoutPaginate(Request $request)
    {
        return $this->ticketRepository->all();
    }

    /**
     *
     * @param  Request $request
     * @return mixed
     */
    public function all(Request $request)
    {
        TicketFilter::apply($this->ticketRepository, $request);

        return $this->ticketRepository->all();
    }

    /**
     *
     * @param  $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->ticketRepository->find($id);
    }

    /**
     *
     * @param  $data
     * @return array
     */
    public function create($data)
    {
        DB::beginTransaction();

        try {
            unset($data['_token']);

            $client = $this->clientService->findByEmail($data['email']);

            if (!$client) {
                $client = new Client();
                $client->fill($data);
                $client->save();
            }

            $order = $this->orderService->findByNumber($data['number']);

            if (!$order) {
                $order = new Order();
                $order->fill($data);
                $order->client_id = $client->id;
                $order->save();
            }

            if ($order->client_id != $client->id) {
                throw new ValidationException(
                    "O Ticket de n° {$order->number} já esta cadastrado para o cliente: 
                    {$order->client->name}"
                );
            }

            $ticket = $this->findTicket($data['number']);


            if (!empty($ticket) && $ticket->ticket_number == $data['number']) {
                $this->update($ticket, $data, $order->id);
            } else {
                $newTicket = new Ticket();

                $newTicket->title = $data['title'];
                $newTicket->note = $data['note'];
                $newTicket->ticket_number = $data['number'];
                $newTicket->order_id = $order->id;

                $newTicket->save();

                DB::commit();
            }

            return [
                'status' => '00'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            return ['status' => '01', 'message' => $e->getMessage()];
        }
    }


    /**
     *
     * @param $ticket
     * @param $data
     * @param $order
     */
    public function update($ticket, $data, $order)
    {
        $ticket->title = $data['title'];
        $ticket->note = $data['note'];
        $ticket->ticket_number = $data['number'];
        $ticket->order_id = $order;

        $ticket->save();

        DB::commit();
    }

    /**
     *
     * @param  $id
     * @return array
     */
    public function delete($id)
    {
        $dir = public_path('../storage/app/public/linha/linha_' . $id);

        if (is_dir($dir)) {
            $this->clearFolder($dir);
        }

        $this->lineRepository->find($id);

        try {
            $this->lineRepository->delete($id);
            return ['status' => '00'];
        } catch (\Exception $e) {
            return ['status' => '01'];
        }
    }

    /**
     *
     * @param  $orderNumber
     * @return mixed
     */
    public function findOrderOfTicket($orderNumber)
    {
        return DB::table('order')
            ->join('ticket', "default.order.id", '=', 'default.ticket.order_id')
            ->where('default.order.number', '=', $orderNumber)
            ->get();
    }

    /**
     *
     * @param  $ticketNumber
     * @return mixed
     */
    public function findTicket($ticketNumber)
    {
        return $this->ticketRepository->findBy('ticket_number', $ticketNumber);
    }
}
