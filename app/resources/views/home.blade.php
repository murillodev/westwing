@extends('layouts.app')

@section('content')
<div class="container" id="home">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="col-lg-12" style="text-align: center">
                        <img id="logo-home" src="https://dzl1a8sxosbf0.cloudfront.net/image/upload/f_webp,fl_awebp,t_default/v1529713811/club/br/controlledbanner/428/1">
                        <h4>Seja bem vindo {{ Auth::user()->name }}.</h4>
                        
                        <a id="create-ticket" href="{{ route('ticket.create') }}">
                            <button id="create-ticket" type="button">
                                Crie um ticket
                            </button>
                        </a>
                    </div>
                </div>
            
        </div>
    </div>
</div>
@endsection
