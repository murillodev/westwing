@extends('layouts.app')

@section('content')
    <div class="container body-background body-detail">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <span>
                        <h4 class="label-detail">N° do Ticket: {{ $ticketNumber }}</h4>
                    </span>
                    <span>
                        <h4 class="label-detail">Nome do Cliente: {{ $clientName }}</h4>
                    </span>
                    <span>
                        <h4 class="label-detail">Email: {{ $clientEmail }}</h4>
                    </span>
                    <span>
                        <h4 class="label-detail">Título do ticket: {{ $ticketTitle }}</h4>
                    </span>
                    <span>
                        <h4 class="label-detail">Conteúdo do ticket: {{ $ticketNote }}</h4>
                    </span>
                    <span>
                        <h4 class="label-detail">Data: {{$ticketDate}}</h4>
                    </span>
                    <div class="col-sm-1 float-right btn-add-ticket">
                        <a href="{{ route('ticket') }}" class="btn btn-primary btn-westwing">Voltar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection