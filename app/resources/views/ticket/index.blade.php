@extends('layouts.app')

@section('content')
    <div class="container body-background body-tickets">

        <div class="row">
            <div class="col-lg-12">
                <h1 style="color:#FFFFFF">Relatório de Tickets:</h1>
            </div>

            <div class="col-lg-12">
                @if($success = session('success'))
                    <div class="alert alert-success">
                        {{ $success}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <input type="email" name="email" class="form-control" placeholder="E-mail" value="{{ request('email') }}">
                                </div>

                                <div class="col-sm-4 ticket-number">
                                    <input type="number" name="number" class="form-control" placeholder="N° do Ticket" value="{{ request('number') }}">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary btn-westwing">Filtrar <i class="fa fa-filter"></i></button>

                                @if(!empty(request('email')) || !empty(request('number')))
                                    <div class="btn btn-danger m-l-sm">
                                        <a href="{{ request()->url() }}" style="color:#fff">Limpar Filtros <i class="fa fa-trash"></i></a>
                                    </div>
                                @endif

                            </div>
                            <div class="col-sm-1 float-right btn-add-ticket">
                                <a href="{{ route('ticket.create') }}" class="btn btn-primary btn-westwing">Adicionar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>N° do Ticket</th>
                                    <th>Nome do Cliente</th>
                                    <th>Email</th>
                                    <th>Título do Ticket:</th>
                                    <th>Conteúdo do ticket:</th>
                                    <th>Data</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($tickets) > 0)
                                    @foreach($tickets as $ticket)
                                        <tr>
                                            <td>
                                                <a href="{{ route('ticket.detail', $ticket->order->number)}}" class="report-td">
                                                    {{ $ticket->order->number }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('ticket.detail', $ticket->number)}}" class="report-td">
                                                    {{ $ticket->order->client->name }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('ticket.detail', $ticket->number)}}" class="report-td">
                                                    {{ $ticket->order->client->email }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('ticket.detail', $ticket->number)}}" class="report-td">
                                                    {{ $ticket->title }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('ticket.detail', $ticket->number)}}" class="report-td">
                                                    {{ $ticket->note }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('ticket.detail', $ticket->number)}}" class="report-td">
                                            {{ (new \Carbon\Carbon($ticket->created_at))->format('d/m/Y H:i:s') }}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Nenhum registro encontrado na Base de Dados.</td>
                                    </tr>
                                @endif
                            </tbody>
                            @if($tickets->total() > 5)
                                <tfoot>
                                <tr>
                                    <td colspan="6" class="footable-visible">
                                        {{ $tickets->appends(request()->input())->links() }}
                                    </td>
                                </tr>
                                </tfoot>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection