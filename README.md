#Westwing test

####Setup:
Obs: To run this project you must have docker (and docker-compose) and git installed in you environment 

Clone this project:

`$ git clone git@bitbucket.org:murillodev/westwing.git`

Go to laradock folder:

`$ cd westwing/laradock`

Copy the env-example to .env

`cp env-example .env`

Run docker-compose to up the containers:

`$ docker-compose up -d nginx mysql phpmyadmin workspace`

Take a coffee...

Enter the workspace container (it has the composer)

`docker exec -it laradock_workspace_1 bash` 

Run composer into project folder:

`cd app && composer install && exit`

Take another coffee...


Go to http://localhost

Make you registration and enjoy!


####phpMyAdmin:

To access phpMyAdmin go to http://localhost:8080 , the project database is "default"

`Server: mysql`

`Username: default`

`Password: secret`

###Check the PSR-2:
If you want check the PSR-2 of the project...

Go to laradock folder:

`$ cd westwing/laradock`

Enter into workspace container:

`docker exec -it laradock_workspace_1 bash`

Run the phpcbf:

`app/vendor/bin/phpcbf --standard=PSR2 /var/www/app/app`

Run the phpcs:

`app/vendor/bin/phpcs --standard=PSR2 /var/www/app/app`


